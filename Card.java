public class Card{
	
	private String suit;
	private String name;
	private int value;
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public Card(String suit, String name, int value){
		this.suit = suit;
		this.name = name;
		this.value = value;
	}
	
	public String toString(){
		return this.suit + " " + this.name; // + " " + this.value;
	}
	
	//Returns a double value representing the "strength" of the card being played
	public double calculateScore(){
		double score = 0;
		score += this.value;
		
		if (suit.equals("Hearts")){
			score += 0.4;
		}
		else if (suit.equals("Spades")){
			score += 0.3;
		}
		else if (suit.equals("Diamonds")){
			score += 0.2;
		}
		else{
			score += 0.1;
		}
		return score;
	}
}