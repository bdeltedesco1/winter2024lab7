import java.util.Scanner;
public class SimpleWar{
	
	public static void main(String[] args){
		GameManager game = new GameManager();
		int round = 0;
		
		int player1Points = 0;
		int player2Points = 0;
		
		System.out.println("*******************************");
		System.out.println("Welcome to a simplified version of the game War!");
		
		//Deals out a card to each player, compares their values to find which 
		//one is larger, then adds one to the score of the winning player
		while (game.getNumberOfCards() >= 0){
			Scanner reader = new Scanner(System.in);
			
			round += 1;
			System.out.println("*******************************");
			System.out.println("Round " + round);
			
			System.out.println(game);
			
			if (game.calculateWinner()){
				player1Points += 1;
			}
			else{
				player2Points += 1;
			}
			
			System.out.println("Player 1 Points: " + player1Points);
			System.out.println("Player 2 Points: " + player2Points);
			
			//Ends the game when there are no more cards left
			if (game.getNumberOfCards() == 0){
				break;
			}
			
			game.dealCards();
			
			//Adds a pause so the players can see what happened
			System.out.println("Press any key to continue");
			String temp = reader.next();
		}
		//Checks who won the game
		if (player1Points > player2Points){
			System.out.println("Congratulations Player 1, you won!");
		}
		else if (player1Points < player2Points){
			System.out.println("Congratulations Player 2, you won!");
		}
		else{
			System.out.println("It was a draw! Well played everyone!");
		}
	}
}