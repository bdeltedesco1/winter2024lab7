public class GameManager{
	private Deck drawPile;
	private Card playerOneCard;
	private Card playerTwoCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.playerOneCard = drawPile.drawTopCard();
		this.playerTwoCard = drawPile.drawTopCard();
	}
	
	public String toString(){
		return "----------------------------\nCenter Card: " + this.playerOneCard + "\nPlayer Card: " + this.playerTwoCard + "\n----------------------------";
	}
	
	//Draws the top two cards and hands them to the two players
	public void dealCards(){
		this.playerOneCard = drawPile.drawTopCard();
		this.playerTwoCard = drawPile.drawTopCard();
	}
	
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	//returns true if player 1 won or false if player 2 won
	public boolean calculateWinner(){
		if (this.playerOneCard.calculateScore() > this.playerTwoCard.calculateScore()){
			return true;
		}
		else{
			return false;
		}
	}
}