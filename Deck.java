import java.util.Random;
public class Deck{
	private Card[] deck;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.numberOfCards = 52;
		
		//Creates a deck of cards with for suits of 13 cards each.
		this.deck = new Card[52];
		String[] suits = new String[]{"Spades", "Clubs", "Hearts", "Diamonds"};
		String[] names = new String[]{"Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
		
		int counter = 0;
		for(int suit = 0; suit < suits.length; suit++){
			for(int i = 0; i < names.length; i++){
				
				this.deck[counter] = new Card(suits[suit], names[i], i+1);
				counter += 1;
				//System.out.println(this.deck[i*(suit+1)]);
			}
		}
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		this.numberOfCards -= 1;
		return this.deck[this.numberOfCards];
		//returns the last card in the array "deck" and reduces the numberOfCards by 1;
	}
	
	public String toString(){
		
		String allCards = "";
		
		for(Card i : this.deck){
			allCards += i + "\n";
		}
		return allCards;
	}
	
	//Shuffles the cards in the deck
	public void shuffle(){
		for (int i = 0; i < this.numberOfCards; i++){
			int randomNumber = rng.nextInt(this.numberOfCards);
			Card storage = this.deck[randomNumber];
			
			this.deck[randomNumber] = this.deck[i];
			this.deck[i] = storage;
		}	
		
		/*for (int i = 0; i < this.numberOfCards; i++){
			System.out.println(this.deck[i]);
		}*/
		
	}
	
}